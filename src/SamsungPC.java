/**
 * Created by amen on 8/8/17.
 */
public class SamsungPC extends AbstractPC {

    public SamsungPC(String computerName, COMPUTER_BRAND brand, int cpu_power, double gpu_power, boolean isOverclocked) {
        super(computerName, brand, cpu_power, gpu_power, isOverclocked);

        System.out.println("Tworzymy kompa: " + this.computerName);
    }

    public AbstractPC createMacPC(){
        return new SamsungPC("Samsung", COMPUTER_BRAND.APPLE, 101, 95, true);
    }
}
